from timeit import default_timer
from random import randint


def func(size):
    return [randint(0, 1000) for _ in range(0, size)]


def points(min_size, max_size):  # Подсчет кол-ва точек
    return [i for i in range(min_size, max_size + 10, 10)]


def bubble(array):  # Пузырьковая сортировка
    start = default_timer()
    for i in range(len(array)):
        for j in range(len(array) - i - 1):
            if array[j] > array[j + 1]:
                array[j], array[j + 1] = array[j + 1], array[j]
    end = default_timer() - start

    return array, end


def insertion(array):  # Сортировка вставками
    start = default_timer()
    for i in range(1, len(array)):
        symb = array[i]
        pos = i - 1

        while pos >= 0 and symb < array[pos]:
            array[pos + 1] = array[pos]
            pos -= 1

        array[pos + 1] = symb
    end = default_timer() - start
    return array, end


def shell(array):  # Сортировка Шелла
    start = default_timer()
    inc = len(array) // 2
    while inc:
        for i, el in enumerate(array):
            while i >= inc and array[i - inc] > el:
                array[i] = array[i - inc]
                i -= inc
            array[i] = el
        inc = 1 if inc == 2 else int(inc * 5.0 / 11)
    end = default_timer() - start

    return array, end


def quick(array, first, last):  # Быстрая сортировка
    start = default_timer()
    if first >= last:
        return

    b, e = first, last
    m = array[(b + e) // 2]
    while b <= e:
        while array[b] < m:
            b = b + 1
        while array[e] > m:
            e = e - 1
        if b <= e:
            array[b], array[e] = array[e], array[b]
            b, e = b + 1, e - 1
    quick(array, first, e)
    quick(array, b, last)
    end = default_timer() - start

    return array, end
