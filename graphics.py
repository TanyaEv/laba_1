import matplotlib.pyplot as plt


def graphic(points, time_bubble, time_insertion, time_shell, time_quick):
    ax = plt.subplot()
    ax.set_title('Оценка сложности алгоритмов по времени')
    ax.plot(points, time_bubble, "-o", label="Пузырьковая", markeredgewidth=0.05)
    ax.plot(points, time_insertion, "-o", label="Вставками", markeredgewidth=0.05)
    ax.plot(points, time_shell, "-o", label="Шелла", markeredgewidth=0.05)
    ax.plot(points, time_quick, "-o", label="Быстрая", markeredgewidth=0.05)
    plt.xlabel('Кол-во элементов в массиве')
    plt.ylabel('Время(с)')
    ax.legend(fontsize=8, ncol=2, facecolor='white', edgecolor='black', title='Сортировки', title_fontsize='8')
    plt.grid()
    plt.show()
